import assert from 'assert'
import chai from 'chai'
import Calculadora from '../src/Calculadora.js'

const expect = chai.expect

describe('Testes de Soma', () => {
    it('Deve somar 4 e 5 resultando em 9', () => {
        let resultado = Calculadora.soma(4, 5);
        expect(resultado).to.be.eq(9)
    });

    it('Deve somar -4 e 5 resultando em 1', () => {
        let resultado = Calculadora.soma(-4, 5);
        expect(resultado).to.be.eq(1);
    });
});

describe('Teste de Subtração', () => {
    it('Deve subtrair 5 e 4 resultando em 1', () => {
        let resultado = Calculadora.subtracao(5, 4);
        expect(resultado).to.be.eq(1);
    });
});

describe('Teste de Multiplicação', () => {
    it.only('Deve multiplicar 5 e 4 resultando em 20', () => {
        let resultado = Calculadora.multiplicacao(5, 4);
        expect(resultado).to.be.eq(20);
    });
});

describe('Teste de divisão', () => {
    it.only('Deve dividir 20 por 4 resultando em 5', () => {
        let resultado = Calculadora.divisao(20, 4);
        expect(resultado).to.be.eq(5);
    });
});