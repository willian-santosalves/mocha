import assert from 'assert'
import chai from 'chai'

const expect = chai.expect

describe('Meu primeiros testes', () => {
    it('Verificar a variável "auxiliar"', () => {
        let auxiliar = 2;
        // assert.strictEqual(auxiliar, 2)
        expect(auxiliar).to.be.equals(2).and.to.be.a('number');
        // expect(auxiliar).to.be.a('number');
    });

    it.only('Verificar uma desigualdade', () => {
        let auxiliar = 2;
        let auxiliar2 = 2;
        // assert.strictEqual(auxiliar, auxiliar2)
        expect(auxiliar).to.be.equals(auxiliar2).and.not.equals('string');
    });

    it('Verificar uma desigualdade', () => {
        let auxiliar = 3;
        let auxiliar3 = true;
        // assert.strictEqual(auxiliar, auxiliar3)
    });
});